<!-- Status Start -->
1. **Stakeholders:**  
1. **DRI:** 
1. **Next Release Date:** 
1. **Status:** 
1. **Stage:** 
1. **Achieved:** 
1. **Features Included in Next Release:** 
1. **Blockers:** 
1. **Overall Estimated Size**: 
<!-- Status End -->

# Work breakdown

| Size | Dedicated Working Time | Points | Examples |
| ------ | ------ | ------ | ------ |
XS | < 1 hour | 1 | Update existing handbook page. #data research/response.
S | < 1 day | 2-3 | New handbook page. Typical triage issue.  New dashboard on top of existing models.
M | ~1 week | 5-8 | New dashboard requiring new models. New data source.
L | 1-3 weeks | 13 | New fact table implementation & testing. Full XMAU solution.
XL | 1-2 months. | 26+ | 

**Data Champion:** 

**Business Sponsor:** 

| Work/activity | Size | Points | Issue/epic Link | DRI |
| ------------- | -----| ------ | ----|----|
| | |  | | 
| |  |  | |
| |  |  |  |
| |  |  | |
|  |  |  |  | 
|  |  |  |  |
| |  |  | | 
|  |  |  | | 
| |  |  |  |  
| ***Total***   | .... |  | ... |

/label ~"Data-Platform-weekly-rollup" 
