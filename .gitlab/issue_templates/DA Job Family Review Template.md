<!-- The below header should be the Title of the Issue -->
# Review DA Job Family Role/Responsibility for [Name] in FYXX-QX 

## Background 
The purpose of this exercise is for individuals within this role to provided feedback on:
*  the current responsiblities are for this role and 
    - whether there is misalignment betweeen expected responsibilities and actual job duties 
    - whether there is misalignment between business expectations of this role and how that affects business engagement strategies 
    - whether there is misalignment between DA <> DE roles and how that affects our efficiency as one Data team 
*  additional changes to the responsibilities of this job family 
*  new engagement strategies 
    - with Data DRIs (business leaders) for `Data Analyst Business Leader Engagement` 
    - with Business Stakeholders (including Analyst or other stakeholder roles) for `Data Analyst Stakeholder Engagement` 
    - with Data Engineers for `Data Analyst <> Data Engineer Engagement`
*  changes to the Data Analyst power tools or manual  

## Current Role Responsibility Feedback 

Review the [Data Analyst Job Family description](https://about.gitlab.com/job-families/finance/data-analyst/) and [this image of Data Management roles](https://cdn.ttgtmedia.com/rms/onlineimages/data_management-comparing_roles.png) and provide feedback in the sections below: 

**For the first exercise, share what you think the Data Analyst role is in managing the:** 
*  Intake process of new issues 
*  Issue Prioritization 
*  Use Case Development 
*  Project Planning 
*  Delivery (Sisense, dbt, etc)

### What should we keep (what do we like in the job description)?
<!-- Please provide feedback here--> 

### What are responsibilites that you perform that are not covered in this job description?
<!-- Please provide feedback here--> 

### How can we improve this job description? 
<!-- Please provide feedback here--> 

### What actions do you commit to in order to ensure the success of rolling out the job description improvement?
<!-- Please provide feedback here--> 

### Additional Feedback 
<!-- Please provide feedback here--> 

## `Data Analyst Business Leader Engagement` Feedback 
This section is to consider how we can improve the working relationship with the Data DRI by providing this feedback to them. 
Please focus on positive constructive feedback that will improve collaboration across teams.

### Provide a brief description of your current business engagement strategy with the Data DRI from your perspective. 
<!-- Please provide feedback here--> 

### How can we improve the current business engagement strategy? (How can the Data DRI help?)
<!-- Please provide feedback here--> 

### What actions do you commit to in order to ensure the success of rolling out an improved business engagement strategy with the Data DRI? (How can you help?)
<!-- Please provide feedback here--> 

### Additional Feedback 
<!-- Please provide feedback here--> 


## `Data Analyst Stakeholder Engagement` Feedback 
This section is to consider how we can improve the working relationship with businesss stakeholders by providing this feedback to them. 
Please focus on positive constructive feedback that will improve collaboration across teams.

### Provide a brief description of your current business engagement strategy with other business stakeholders from your perspective. 
<!-- Please provide feedback here--> 

### How can we improve the current business engagement strategy? (How can the business stakeholders help?)
<!-- Please provide feedback here--> 

### What actions do you commit to in order to ensure the success of rolling out an improved business engagement strategy with the business stakeholders? (How can you help?)
<!-- Please provide feedback here--> 

### Additional Feedback 
<!-- Please provide feedback here--> 


## `Data Analyst <> Data Engineer Engagement` Feedback 
This section is to consider how we can improve the working relationship with the Data Engineering team by providing this feedback to them. 
Please focus on positive constructive feedback that will improve collaboration across teams.

### Provide a brief description of your current engagement with the Data Engineering team. 
<!-- Please provide feedback here--> 

### How can we improve the current Data Engineering engagement strategy? (How can the data engineers help?)
<!-- Please provide feedback here--> 

### What actions do you commit to in order to ensure the success of rolling out an improved Data Engineering engagement strategy? (How can you help?)
<!-- Please provide feedback here--> 

### Additional Feedback 
<!-- Please provide feedback here--> 


## Review of Data Analyst Power Tools 
This section is to consider what power tools you need to be a successful Data Analyst here at GitLab. 
A `power tool` is any template or structure that helps you navigate the day-to-day work, examples include data team handbook pages, project plan template, etc. 

### What are your favorite Data Analyst Power Tools ? 
<!-- Please provide feedback here--> 

### Which of the Data Analyst Power Tools can be improved? How? 
<!-- Please provide feedback here--> 

### Additional Feedback 
<!-- Please provide feedback here--> 

## Review of Data Analyst Manual 
As we build out the data analyst manual, please consider providing feedback on what information is helpful for the success of your and your team memmber growth here at GitLab. 

### What are your favorite part of the Data Analyst Manual? 
<!-- Please provide feedback here. If it does not exist, please move on to the next question. --> 

### How can the Data Analyst Manual be improved for you? For the stakeholders? 
<!-- Please provide feedback here--> 

### Additional Feedback 
<!-- Please provide feedback here--> 

/label ~Analytics ~"Data Team" ~Housekeeping ~Documentation ~"workf   low:In dev"
/weight 1
/confidential 

