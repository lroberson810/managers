## Daily Checklist for Data Engineering

- [ ] Check on approved, ready-for-provisioning access request [issues](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues?scope=all&utf8=%E2%9C%93&state=opened&assignee_username[]=jjstark&label_name[]=ReadyForProvisioning&label_name[]=ManagerApproved) 
- [ ] check triage activity and completeness
- [ ] check [milestone progress](https://gitlab.com/groups/gitlab-data/-/boards/1373923)
- [ ] review and merge ready [MRs](https://gitlab.com/dashboard/merge_requests?assignee_username=jjstark)
- [ ] check for new [permifrost issues](https://gitlab.com/gitlab-data/permifrost/-/issues)
