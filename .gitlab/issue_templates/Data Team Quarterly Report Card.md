<!-- The header below should be the Title of the Issue -->
# `FYXX-QX` Data Team Quarterly Report Card

| Area | Grade | Notes |
| ------ | ------ | ------ |
| Great Team | `GRADE` | <!-- Provide notes here-->  It all starts with people. Our values are CREDIT. Our mission is to **Deliver Results That Matter With Trusted and Scalable Data Solutions**. We'll measure our progress towards operating as a high-performance results-oriented team - One Team - and helping everyone achieve their data goals. |
| Dashboards & Self-Service Enablement | `GRADE` | <!-- Provide notes here-->  Measure progress of our progress towards developing Self-Service capabilities, including self-service dashboard development and self-service SQL. Also includes training, Data Catalog development, and other enablement activities. |
| KPI Development | `GRADE` | <!-- Provide notes here-->  Measure our progress towards streamlining KPI development and governance. |
| Enterprise Dimensional Model | `GRADE` | <!-- Provide notes here-->  Measure progress of our new Kimball-design Enterprise Dimensional Model, which is the unlock for delivering consistent trusted data and Sisense Data Discovery. |
| Pipes & Pumps | `GRADE` | <!-- Provide notes here-->  Measure progress towards building a high-performance and reliable Snowflake Data Warehouse, including Data Source SLO adherence, new data pipeline additions (the "blood" of new analytics) and new data pump additions (improving business systems). |