## Promotion: [Full Name] to [Level] Data Analyst 

### Overview 

In this section, share information on: 
1. Who is the person being promoted? (Link to GitLab Profile and Current Title)
1. How long has this person been here at GitLab? 
1. What division/project does this person support? 

<!-- 
    Ex: 
    Ashley joined GitLab on YYYY-MM-DD as a [Current Title] supporting the [Business/Project]. 
    Ashley consistently delivers great data quality reports .... etc... 
-->

### Achievements and Technical Competencies 
In this section, for each item on the [Data Analyst Job family description](https://about.gitlab.com/job-families/finance/data-analyst/), share issues/MRs that show this person has performed above and beyond for this [level] of the Data Analyst job family. 

<!-- 
    Example: for the responsibility "Collaborate with other functions across the company by building reports and dashboards with useful analyses and strong data insights" 
    -  Ashley proactively seeks opportunities to collaborate cross-functionally to build dashboards that drive business decisions: 
        - Ashley created the [Marketing Funnel Dashboard](Issue) in [Sisense](Link to Dashboard) that was used to change the [lead process](link to handbook)
        - Ashley set up [weekly check-ins with Business DRIs](Issue) to discuss efficiency plans. 
-->


### [CREDIT](https://about.gitlab.com/handbook/values/) Values Alignment 

#### Collaboration 
List examples of how this person displays the collaboration value.


#### Results 
List examples of how this person displays the results value.


#### Efficiency 
List examples of how this person displays the efficiency value.


#### Diversity & Inclusion
List examples of how this person displays the diversity & inclusion value.


#### Iteration  
List examples of how this person displays the iteration value.


#### Transparency   
List examples of how this person displays the transparency value. 


### Leadership 
List examples of how this person displays the leadership value. 


### Recognition

#### Internal 
List of Bonuses or shoutouts 

#### External 
List of external references  


### References 
Reach out to other GitLabbers who supports this promotion to share:
-  How this person outperforms their current role
-  How this person lives the GitLab values 
-  Why this person deserves this promotion 


#### Reference 1

#### Reference 2

#### Reference 3


